'''
#phi_discimg-out
#processes intermediate dng files to tiff
#moves all image files to qcDir
'''
import configparser
import argparse
import subprocess
import os
import sys
import time
import getpass
import re
import rawpy
import imageio
import pyodbc #Mac has issues with pypyodbc, and pyodbc works fine
#import pypyodbc as pyodbc
from PIL import Image
from distutils import spawn
###UCSB modules###
import config as rawconfig
import util as ut
import logger as log
import mtd
import makestartobject as makeso

def idSize(fname,endDir):
	'''
	images come in off the camera in the wrong orientation
	this returns the correct orientation, given that the image should be landscape
	and image is never at correct orientation as it comes off camera
	'''
	#06-01-2023 The current camera setup takes the image in the correct orientation
	#While this function is not currently needed, it may be required should the camera setup change again
	img = Image.open(os.path.join(endDir,fname + ".tif"))
	if img.width > img.height:
		rotation = 180
	else:
		rotation = 90
	if rotation:
		return rotation
	else:
		print ("buddy, something's up")
		log.log({"message":"something is wrong with the rotation calculation","level":"error"})
		sys.exit()

def rawpyToTif(startObjFP,fname,endDir):
	'''
	converts the raw cr2 file to tiff
	'''
	#output = subprocess.check_output(['gm','convert',startObjFP,'-rotate',rotation,'-crop','3648x3648+920','-density','300x300',os.path.join(endDir,fname + ".tif")])
	with rawpy.imread(startObjFP) as raw:
		rgb = raw.postprocess(use_camera_wb=True)
	imageio.imsave(os.path.join(endDir,fname + ".tif"),rgb)

def rotateTif(startObjFP,fname,rotation,endDir):
	'''
	rotates the tiff
	'''
	img = Image.open(os.path.join(endDir,fname + ".tif"))
	img2 = img.rotate(rotation,expand=True)
	img2.save(os.path.join(endDir,fname + "-rotated.tif"))
	#! More testing is required to see if this sleep statement is necessary, or can be reduced !
	time.sleep(2)
	os.remove(os.path.join(endDir,fname + ".tif"))
	os.rename(os.path.join(endDir,fname + "-rotated.tif"),os.path.join(endDir,fname + ".tif"))

def cropTif(startObjFP,fname,endDir):
	'''
	crops the tif to largest possible square
	'''
	img = Image.open(os.path.join(endDir,fname + ".tif"))
	#Creating a shallow copy of image, as any changes to image happen to the actual file
	img2 = img
	width = img.width
	height = img.height
	if img.height>img.width:
		#In this case the image is in portrait orientation
		#This is usually caused by a misinterpretation of the RAW file's orientation value
		img2 = img2.rotate(-90,expand=True)
		width = img.height
		height = img.width

	#The result image is ideally a square
	#Side length of square is height of image
	#The square should be centered along the width

	#excess is pixel along width that are being cropped
	#split the excess evenly across the left and right side of crop
	excess = (width - height)/2

	#set x-coord for left side of square
	left = excess

	#set y-coord for top of square (y coord starts from top and goes downward)
	top = 0

	#set x-coord for right side of square
	right = excess+height

	#set y-coord for bottom of square
	bottom = height

	#crop the image, create square from (left,upper) point to (right,lower) point
	img2 = img2.crop((left,top,right,bottom))

	#replace tiff with cropped version
	img2.save(os.path.join(endDir,fname + "-cropped.tif"))
	os.remove(os.path.join(endDir,fname + ".tif"))
	os.rename(os.path.join(endDir,fname + "-cropped.tif"),os.path.join(endDir,fname + ".tif"))
	output = subprocess.check_output([conf.python,os.path.join(conf.scriptRepo,'hashmove.py'),'-nm',os.path.join(endDir,fname + ".tif")])
	#log.log(output)

def tifToJpg(startObjFP,fname,endDir):
	'''
	converts the tiff to jpeg
	'''
	img = Image.open(os.path.join(endDir,fname + ".tif"))
	img.save(os.path.join(endDir,fname + ".jpg"),"JPEG",quality=100)
	#output = subprocess.check_output(['gm','convert',os.path.join(endDir,fname + ".tif"),'-resize','800x800',os.path.join(endDir,fname + ".jpg")])
	#with rawpy.imread(startObjFP) as raw:
		#rgb = raw.postprocess(use_camera_wb=True)
	#imageio.imsave(os.path.join(endDir,fname + ".jpg"),rgb)
	output = subprocess.check_output([conf.python,os.path.join(conf.scriptRepo,'hashmove.py'),'-nm',os.path.join(endDir,fname + ".jpg")])
	#log.log(output)

def moveSO(startObjFP,endDir):
	'''
	hashmoves the original cr2 to pre-ingest-qc
	'''
	output = subprocess.check_output([conf.python,os.path.join(conf.scriptRepo,'hashmove.py'),startObjFP,endDir])
	#log.log(output)

def mark_processed_FM(file):
	sqlstr = """update SONYLOCALDIG set imageProcessed='1' where filename='""" + file + """'"""
	conn = pyodbc.connect(conf.NationalJukebox.cnxn)
	conn.setencoding(encoding='utf-8')	
	mtd.insertFM(sqlstr, conn)

def main():
	'''
	do the thing
	'''
	global conf
	conf = rawconfig.config()
	parser = argparse.ArgumentParser(description="processes image files for disc labels")
	parser.add_argument('-i','--input',dest='i',help="the rawcapture file.cr2 to process, not full path")
	parser.add_argument('-m','--mode',dest='m',choices=["single","batch"],help='mode, process a single file or every file in capture directory')
	args = parser.parse_args()
	#imgCaptureDir = conf.NationalJukebox.VisualArchRawDir
	imgCaptureDir = args.i
	#log.log("started")
	if args.m == "single":
		startObj = startObjFP = args.i.replace("\\","/")
		if not startObj.startswith(imgCaptureDir):
			for dirs,subdirs,files in os.walk(imgCaptureDir):
				for f in files:
					if f == startObj:
						startObjFP = os.path.join(dirs,startObj)
						break
			if not os.path.exists(startObjFP):
				print (("Object " + startObj + " does not exist in " + imgCaptureDir))
				foo = input("You should check on that or try a different filename")
				sys.exit()
		else:
			startObjFP = startObj
		fname,ext = os.path.splitext(startObj)
		endDir = os.path.join(conf.NationalJukebox.PreIngestQCDir,fname)
		if not os.path.exists(endDir):
			os.makedirs(endDir)


		#convert to tif
		rawpyToTif(startObjFP,fname,endDir)

		#get the orientation of the image and set output rotation accordingly
		#rotation = idSize(fname,endDir)
		#print("Rotation:",rotation)

		#rotate the tif
		#rotateTif(startObjFP,fname,rotation,endDir)

		#06-01-2023 Above two snippets have been commented out
		#The current camera setup takes photos in the correct orientation

		#crop the tif
		cropTif(startObjFP,fname,endDir)

		#convert to jpg
		#should make jpeg from tiff moving forward
		tifToJpg(startObjFP,fname,endDir)

		#move startObj
		moveSO(startObjFP,endDir)

		#mark in FM that it's been processed
		mark_processed_FM(args.i.replace("cusb_","ucsb_").replace(".cr2",""))

		#see if it's ready to qc
		subprocess.call([conf.python, os.path.join(conf.scriptRepo, "makesip.py"), "-m", "nj", "-i", args.i.replace(".cr2","")])
	
		#make derivative if it is in 15000 folder
		if os.path.isdir("/Volumes/national_jukebox/in_process/15000/"+fname):
			subprocess.call([conf.python, os.path.join(conf.scriptRepo, "makeDerivativesForWeb.py"), "--source", "/Volumes/national_jukebox/in_process/15000"])

	elif args.m == "batch":
		for dirs,subdirs,files in os.walk(imgCaptureDir):
			for f in files:
				if not f.endswith(".DS_Store"):
					startObj = f
					startObjFP = os.path.join(dirs,f)
					fname,ext = os.path.splitext(f)
					print("Name of file: ",fname)
					endDir = os.path.join(conf.NationalJukebox.PreIngestQCDir,fname)
					if not os.path.exists(endDir):
						os.makedirs(endDir)
					print (f)
					print(startObjFP,endDir)

					#convert to tif
					rawpyToTif(startObjFP,fname,endDir)
				
					#get the orientation of the image and set output rotation accordingly
					#rotation = idSize(fname,endDir)

					#rotate the tif
					#rotateTif(startObjFP,fname,rotation,endDir)

					#06-01-2023 Above two snippets have been commented out
					#The current camera setup takes photos in the correct orientation

					#crop the tif
					cropTif(startObjFP,fname,endDir)

					#convert to jpg
					#should make jpeg from tiff moving forward
					tifToJpg(startObjFP,fname,endDir)

					#move startObj
					moveSO(startObjFP,endDir)

					#mark in FM that it's been processed
					mark_processed_FM(f.replace("cusb_","ucsb_").replace(".cr2",""))

					#check if it's ready to qc
					subprocess.call([conf.python, os.path.join(conf.scriptRepo, "makesip.py"), "-m", "nj", "-i", f.replace(".cr2","")])
					
	#make derivatives
					
	subprocess.call([conf.python, os.path.join(conf.scriptRepo, "makeDerivativesForWeb.py"), "--source", "/Volumes/national_jukebox/in_process/15000"])
if __name__ == '__main__':
	main()
	#log.log("complete")
