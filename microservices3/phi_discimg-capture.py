'''
#nj_discimg-capture-fm
#triggered by filemaker, takes 1 argument for barcode that was scanned into FM
'''

import glob
import os
import sys
import argparse
import subprocess
import time
import pyodbc #01-26-2023 Importing pypyodbc instead as Windows has issues with pyodbc
#import pypyodbc as pyodbc
import configparser
###UCSB modules###
#import gphoto_interface
import config as rawconfig
#import logger as log
import util as ut
import mtd
from time import sleep


def main():
	'''
	do the thing
	'''
	#initialize via the config file
	print("imports done")
	#gphoto_interface.processBarcode('ucsb_col_3112_081_71083_00')
	global conf
	conf = rawconfig.config()
	parser = argparse.ArgumentParser(description="capture, import, and rename a photo for the PHI project")
	parser.add_argument('barcode', help="the barcode for the disc side you want to capture")
	args = parser.parse_args()
	rawCapturePath = conf.NationalJukebox.VisualArchRawDir
	if not os.path.exists(rawCapturePath):
		os.makedirs(rawCapturePath)
	barcode = args.barcode.strip() #grab the lone argument that FM provides
	barcode = barcode.replace("ucsb", "cusb") #stupid, stupid bug
	fname = barcode + ".cr2" #make the new filename
	#log.log("started")
	print (conf.python)
	#print (os.path.join(conf.scriptRepo, "capture-image.py"))
	subprocess.call([conf.python,os.path.join(conf.scriptRepo,"capture-image.py"), "-nj"])
	with ut.cd(rawCapturePath): #cd into capture dir
		if os.path.isfile(os.path.join(rawCapturePath, barcode + ".cr2")) or os.path.isfile(os.path.join(rawCapturePath, barcode+ ".CR2")): #error checking, if the file already exists
			#log.log(**{"message":"It looks like you already scanned that barcode " + barcode, "level":"warning"})
			print ("It looks like you already scanned that barcode")
			a = input("hit ENTER to continue scanning")
			sys.exit()
		newest = max(glob.iglob('*.[Cc][Rr]2'), key=os.path.getctime) #sort dir by creation date of .cr2 or .CR2 files
		if newest[:3]!="cap":
			print("Attempting to rename barcode file, please rescan record")
			sys.exit()
		os.rename(newest, fname) #rename the newest file w/ the barcode just scanned
		#log.log("renamed " + newest + " " + fname)
		try:
			conn = pyodbc.connect(conf.NationalJukebox.cnxn)
			conn.setencoding(encoding='utf-8')
			mtd.insertFM("""update SONYLOCALDIG set capturedImage='1' where filename='""" + args.barcode.strip() + """'""", conn)
		except Exception as exc:
			print(exc)
			input("error occurred, please rescan record, press ENTER to continue")
			sys.exit()

if __name__ == '__main__':
	main()
	#log.log("complete")
