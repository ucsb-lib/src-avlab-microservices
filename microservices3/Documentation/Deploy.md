## Deployment guide
#### for University of California Santa Barbara Library Special Resource Collections Audio Visual Lab _microservices_ python package

To deploy on Windows you'll need:

- Python 3.11 or higher
  - including the _pip_ package manger
- git client software

### Install av_lab_microservices3 python package

##### Run powershell as an admin.

To install the package at the system level and thus make it available to all users who log in you'll need to run the pip command below as an administrator.

```
pip install -e "git+https://gitlab.com/ucsb-lib/src-avlab-microservices.git@master#egg=av_lab_microservices3&subdirectory=microservices3"
```

***`-e`*** so a full copy of the git repo is created/retained on the machine

***`@master`*** to get the master branch of the git repo
***`#egg=av_lab_microservices3&subdirectory=microservices3`*** to properly name the package and installs from the specific sub directory

Pip will run for a while and near the end of pip's output you should see:  
> Successfully built av_lab_microservices3  

After pip completes its run you can run:

```
pip list
```

and the output should be something like:

```
Package               Version Editable project location
--------------------- ------- ------------------------------------------------------------
av_lab_microservices3 2.1.1   C:\WINDOWS\system32\src\av-lab-microservices3\microservices3
bagit                 1.8.1
beautifulsoup4        4.12.3
bs4                   0.0.2
config                0.5.1
configparser          7.1.0
ff                    0.0.10
imageio               2.36.1
lxml                  5.3.0
markdown-it-py        3.0.0
mdurl                 0.1.2
numpy                 2.2.0
pillow                11.0.0
pip                   24.3.1
psutil                6.1.0
Pygments              2.18.0
pyodbc                5.2.0
pypyodbc              1.3.6
setuptools            75.6.0
soupsieve             2.6
```

Your version numbers may not exactly match those above but that should not be a problem.

At this point one can exit the powershell runing as admin.

##### Add to `Path` environment variable

I'm not sure if this step is required or not but I added the last 3 items in this screenshot to my Windows Path system environment variable.

![](images/edit-path-env-var-windows_scrnshot.png)

The file paths will likely be different than what you see in the above screenshot.  What you want is to have `Python\Lib\site-packages\pip` and the "Editable project Location" as part of the system's Path environment variable.

##### Testing

To test if things are minimally working one can now run one of the python scripts and verify it doesn't throw errors.  

Here we'll run the `phi_audio.py` script with a dummy argument of `12345`

```
python C:\Windows\System32\src\av-lab-microservices3\microservices3\phi_audio.py 12345
```

expected response:

```
C:\Windows\System32\src\av-lab-microservices3\microservices3\util.py:45: SyntaxWarning: invalid escape sequence '\P'
  return "C:\Program Files\Python311\python.exe"
C:\Windows\System32\src\av-lab-microservices3\microservices3\mtd.py:430: SyntaxWarning: invalid escape sequence '\('
  match = re.search("by.*\(side A\)(\s|\.)",sf.string) #grip just the side A part
C:\Windows\System32\src\av-lab-microservices3\microservices3\mtd.py:437: SyntaxWarning: invalid escape sequence '\('
  match = re.search("\(side A\).*\(side B\)",sf.string) #grip if there is a side b
processing 12345
file 12345.wav missing from arch or broad dir, not processed
Please check that the file was named correctly and saved to the correct directory
```

We can ignore the SyntaxWarnings about invalid escape sequences.  (I'll address those in a later update.)  We expect the `not processed` message because we're not using a real collection item identifier.

