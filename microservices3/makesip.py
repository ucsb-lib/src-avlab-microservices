#! /usr/bin/env python
'''
nj_pre-SIP
verifies that all relevant filetypes exist (and, one day, conform) to NJ SIP
'''
import getpass
import os
import subprocess
import argparse
import re
import pyodbc #Mac works better with original pyodbc
#import pypyodbc as pyodbc
###UCSB modules###
import config as rawconfig
import util as ut
import logger as log
import mtd
import makestartobject as makeso
import makeDerivativesForWeb as makemp3

class mp3ArgsReplacement:
    normalizationScheme = "ebu"

def verify_package_contents(**kwargs):
    '''
    checks that every file type is in the SIPable dir
    '''
    args = ut.dotdict(kwargs)
    packageIsComplete = {}
    for filetype in args.filetypes:
        packageIsComplete[filetype]=False
        for file in os.listdir(args.fullpath):
            if file.endswith(filetype):
                packageIsComplete[filetype]=True
                continue
    for pic,v in packageIsComplete.items():
        if v is False:
            return False
    return True

def move_package_toRepo(**kwargs):
    '''
    Run hashmove as a synchronous subprocess to move the recordings files to a batch dir
    '''
    args = ut.dotdict(kwargs)
    output = subprocess.run(
    ['python', os.path.join(args.scriptRepo, 'hashmove.py'), args.fullpath, os.path.join(args.repo, args.assetName)],
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
    text=True 
    )

    foo,err = output.communicate()
    if err:
        print ( err )
        return False
    else:
        foo = str(foo)
        print ( foo )
        sourcehash = re.search(r'srce\s\S+\s\w{32}',foo)
        desthash = re.search(r'dest\s\S+\s\w{32}',foo)
        dh = desthash.group()
        sh = sourcehash.group()
        if sh[-32:].lower() == dh[-32:].lower():
            return True
        else:
            return False

def make_assetName_fullpath(startObj):
    '''
    returns the canonical assetName and fullpath to SIPable directory
    '''
    #if not (startObj.startswith("R:/") or startObj.startswith("//svmwindows")) and not os.path.isdir(startObj):
    if not os.path.isdir(startObj):
        sobj = makeso.parse_input(startObj)
        assetName = os.path.basename(os.path.dirname(sobj))
        fullpath = sobj.replace(os.path.basename(sobj),"")[:-1]
    else:
        assetName = os.path.basename(os.path.normpath(startObj))
        fullpath = startObj
    return assetName, fullpath

def main():
    '''
    do the thing
    '''
    ###INIT VARS###
    #log.log("started")
    global conf
    conf = rawconfig.config()
    parser = argparse.ArgumentParser(description="makes a SIP")
    parser.add_argument('-i','--input',dest='i',help="the directory or asset name which you would like to SIP")
    parser.add_argument('-m','--mode',dest='m',choices=['nj'],help="mode, the type of SIP to make")
    args = parser.parse_args()
    ###END INIT###
    if args.m == 'nj':
        ###init kwargs objects###
        kwargs = {"materialType":"nj","new_ingest":conf.NationalJukebox.PreIngestQCDir,"repo":conf.NationalJukebox.BatchDir,"scriptRepo":conf.scriptRepo}
        kwargs['filetypes'] = ['m.wav','.wav','.tif','.cr2','.jpg']
        kwargs['assetName'], kwargs['fullpath'] = make_assetName_fullpath(args.i)
        ###end init###
        packageIsComplete = verify_package_contents(**kwargs)
        if packageIsComplete:
            moveSuccess = move_package_toRepo(**kwargs)
            if moveSuccess:
                conn = pyodbc.connect(conf.NationalJukebox.cnxn)
                conn.setencoding(encoding='utf-8')
                mtd.insertFM("""update SONYLOCALDIG set readyToQC='1' where filename='""" + args.i.replace("cusb_","ucsb_") + """'""", conn)
                dirName = os.path.join(kwargs['repo'],args.i)
                fileList = os.listdir(dirName)

                #makesip attempts to create mp3 if the appropriate wav file is available
                #! makedip currently does not make mp3 !
                #! if photo is taken before digitization makeDerivative will need to be run !
                #! mp3 creation should be attempted in makedip to avoid manual running of makeDerivate !
                mp3Args = mp3ArgsReplacement
                print("making mp3 at:",dirName)
                if(any(makemp3.isBroadcastWav(n) for n in fileList)):
                    broadcastWav = makemp3.pickBroadcastWav(fileList)
                    makemp3.makeMp3(dirName, broadcastWav, mp3Args)
                elif(any(makemp3.isNonMasterWav(n) for n in fileList)):
                    ## no broadcast wav in dir to serve as source so we search for plain .wav
                    sourceWav = makemp3.pickSourceWav(fileList)
                    makemp3.makeMp3(dirName, sourceWav, mp3Args)
                else:
                    print("skipping " + dirName + "/ as no .WAV  was found in this directory to use as a source." + "\n")
            else:
                print ( "There was a problem moving the SIP to the repo" )
        else:
            print ( "The supplied package is not complete, no SIP made" )


if __name__ == '__main__':
    main()
    #log.log("complete")
