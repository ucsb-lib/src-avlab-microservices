import subprocess
import os
import configparser
from PIL import ImageShow
from PIL import Image, ImageDraw
from PIL import ImageWin
from time import sleep

def checkBarcode(barcode,location):
	barcode_path = os.path.join(location,(barcode+".jpg"))
	#print(barcode_path)
	return os.path.isfile(barcode_path)

def captureBarcode(barcode,location):
	subprocess.run(["gphoto2","--capture-image-and-download","--filename="+location+barcode+".jpg"])
    
def processBarcode(barcode, location):
	captureBarcode(barcode,location)
	fileMade = checkBarcode(barcode,location)
	print("File Exists? "+str(fileMade))
	return fileMade

def captureImage(location):
	# f = open("c:/processing/text.txt","w+")
	# f.write("Test writing to file")
	# f.close()
	#print("gphoto2","--port \"usb:001,007\"","--camera \"Canon EOS 5DS R\"","--capture-image-and-download","--filename="+location,"--debug","--debug-logfile=c:/processing/debug_file.txt")
	# subprocess.call(["gphoto2","-L"])
	# sleep(2)
	# subprocess.call(["gphoto2","--auto-detect"])
	# sleep(2)
	#subprocess.call(["gphoto2","-P","--debug","--debug-logfile=c:/processing/my-logfile.txt"])
	subprocess.call(["gphoto2","--capture-image-and-download","--filename="+location,"--debug","--debug-logfile=c:/logs/debug_file.txt"],shell=True,cwd="C:/processing")
	#cmd = "gphoto2 --capture-image-and-download --filename="+location+" --debug --debug-logfile=c:/processing/debug_file.txt"
	#os.system(cmd)

def livePreview(location):
	#Location required as temporary place to store photos
	path = os.path.join(location,"capture_preview.jpg")
	for i in range(5):
		subprocess.call(["gphoto2","--show-preview"],cwd=location)
		#sleep(10)
		image = Image.open(path)
		draw = ImageDraw.Draw(image)
		draw.ellipse([(image.size[0]/2)-20,(image.size[1]/2)-20,(image.size[0]/2)+20,(image.size[1]/2)+20])
		draw.line([image.size[0]/2,0,image.size[0]/2,image.size[1]])
		draw.line([0,image.size[1]/2,image.size[0],image.size[1]/2])
		ImageShow.show(image)
		#ImageWin.ImageWindow(image,title="PIL")
		#sleep(5)
		#image = cv2.imread("path",cv2.IMREAD_ANYCOLOR)
		#cv2.imshow("Live View",image)
		os.remove(path)

if __name__ == "__main__":
	livePreview("C:/processing")